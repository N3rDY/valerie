<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('city_id')->unsigned();
            $table->bigInteger('clinic_id')->unsigned();
            $table->bigInteger('doctor_id')->unsigned();

            $table->date('date_of_visit');

            $table->string('service');

            $table->string('notes');
            $table->string('status');
            $table->boolean('is_completed')->default(0);

            $table->foreign('user_id')->references('id')->on('users')->onCascade('delete');
            $table->foreign('city_id')->references('id')->on('cities')->onCascade('delete');
            $table->foreign('clinic_id')->references('id')->on('clinics')->onCascade('delete');
            $table->foreign('doctor_id')->references('id')->on('users')->onCascade('delete');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
