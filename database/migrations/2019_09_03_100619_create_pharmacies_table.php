<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePharmaciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pharmacies', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('name');
            $table->string('address');
            $table->string('town');
            $table->bigInteger('city_id')->unsigned();
            $table->string('email');
            $table->string('phone');
            $table->string('photo');
            $table->timestamps();

            $table->foreign('city_id')->references('id')->on('cities')->onCascade('delete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pharmacies');
    }
}
