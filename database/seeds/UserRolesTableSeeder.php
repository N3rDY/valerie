<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $table = 'roles';
        DB::table($table)->delete();
        DB::table($table)->insert(
            ['id' => 1, 'name' => 'admin']
        );
        DB::table($table)->insert(
            ['id' => 2, 'name' => 'doctor']
        );
        DB::table($table)->insert(
            ['id' => 3, 'name' => 'patient']
        );
    }
}
