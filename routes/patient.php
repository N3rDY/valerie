<?php

Route::group(['middleware' => ['auth', 'patient']], function(){

    Route::get('/', 'PatientController@dashboard')->name('patient.dashboard');

    Route::group(['prefix' => 'profile'], function(){
        Route::get('/', 'PatientController@profile')->name('patient.profile');
        Route::post('/', 'PatientController@update_profile')->name('patient.profile');

        Route::get('/settings', 'PatientController@settings')->name('patient.profile.settings');
        Route::post('/settings', 'PatientController@profileInfo')->name('patient.profile.info');
    });

    Route::group(['prefix' => 'diagnosis'], function(){
        Route::get('/', 'PatientController@diagnosis')->name('patient.diagnosis.list');

        Route::get('/new', 'PatientController@newDiagnosis')->name('patient.diagnosis.new');
        Route::post('/new', 'PatientController@storeDiagnosis')->name('patient.diagnosis.new');
    });

    Route::group(['prefix' => 'appointment'], function(){
        Route::get('/', 'PatientController@schedule')->name('patient.appointment.list');

        Route::get('/new', 'AppointmentController@bookForm')->name('patient.appointment.book');
        Route::post('/new', 'AppointmentController@book')->name('patient.appointment.book');
    });


});
