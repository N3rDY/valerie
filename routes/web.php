<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\CityController;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
    //Cookie::queue(Cookie::make('test', 'abc', 1));
    return view('welcome');
});

Route::get('/no-permission', function (){
    return "Error";
})->name('nopermission');

Route::post('/clinics', 'CityController@getClinics')->name('clinic.list');

Route::post('/doctors', 'AvailabilityController@getDoctors')->name('doctor.list');

Route::post('/availability', 'AvailabilityController@getAvailability')->name('available.list');

Route::post('/symptoms', 'DiagnosisController@getSymptoms')->name('symptoms.list');

Route::post('/conditions', 'DiagnosisController@getConditions')->name('diagnosis.condition');
