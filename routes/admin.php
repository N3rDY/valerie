<?php

Route::group(['middleware' => ['auth', 'admin']], function(){

    Route::get('/', 'AdminController@dashboard')->name('admin.dashboard');

    Route::group(['prefix' => 'profile'], function(){
        Route::get('/', 'AdminController@profile')->name('admin.profile');
        Route::post('/', 'AdminController@update_profile')->name('admin.profile');

        Route::get('/settings', 'AdminController@settings')->name('admin.profile.settings');
        Route::post('/settings', 'AdminController@profileInfo')->name('admin.profile.info');
    });

    Route::group(['prefix' => 'doctor'], function(){
        Route::get('/', 'DoctorController@list')->name('admin.doctor.list');
        Route::get('/new', 'DoctorController@showAddForm')->name('admin.doctor.new');
        Route::post('/new', 'DoctorController@add')->name('admin.doctor.new');

        Route::get('/view/{id}', 'DoctorController@view')->name('admin.doctor.view');
        //Route::get('/view/{id}/schedule', 'DoctorController@view')->name('admin.doctor.view');

        Route::delete('/view/{id}', 'DoctorController@destroy')->name('admin.doctor.destroy');

    });

    Route::group(['prefix' => 'clinic'], function(){

        Route::get('/', 'ClinicController@list')->name('admin.clinic.list');
        Route::get('/new', 'ClinicController@showAddForm')->name('admin.clinic.new');

        Route::post('/new', 'ClinicController@add')->name('admin.clinic.new');

        Route::get('/{id}/doctors', 'ClinicController@doctors')->name('admin.clinic.doctors');

        Route::get('/view/{id}', 'ClinicController@view')->name('admin.clinic.view');

        Route::delete('/view/{id}', 'ClinicController@destroy')->name('admin.clinic.destroy');

        Route::get('/view/{id}/assign', 'ClinicController@assignForm')->name('admin.clinic.assign');
        Route::post('/view/{id}/assign', 'ClinicController@assign')->name('admin.clinic.assign');

        Route::get('/view/{id}/remove', 'ClinicController@removeForm')->name('admin.clinic.remove');
        Route::post('/view/{id}/remove', 'ClinicController@remove')->name('admin.clinic.remove');

    });

    Route::group(['prefix' => 'manager'], function(){
        Route::get('/', 'ManagerController@list')->name('admin.manager.list');
        Route::get('/new', 'ManagerController@add')->name('admin.manager.new');

        Route::delete('/new/{id}', 'AdminController@newDiagnosis')->name('admin.manager.view');
    });

    Route::group(['prefix' => 'pharmacy'], function(){
        Route::get('/', 'PharmacyController@list')->name('admin.pharmacy.list');

        Route::get('/new', 'PharmacyController@showAddForm')->name('admin.pharmacy.new');
        Route::post('/new', 'PharmacyController@savePharmacy')->name('admin.pharmacy.new');

        Route::get('/orders', 'PharmacyController@orders')->name('admin.pharmacy.orders');

        Route::get('/view/{id}', 'PharmacyController@view')->name('admin.pharmacy.view');
        Route::delete('/view/{id}', 'PharmacyController@destroy')->name('admin.pharmacy.destroy');
    });


});
