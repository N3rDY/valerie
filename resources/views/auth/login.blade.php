@extends('auth.auth')

@section('content')
    <section id="wrapper" class="login-register">
        <div class="login-box">
            <div class="white-box">
                <form class="form-horizontal form-material" method="post" id="loginform" action="{{ route('login') }}">
                    @csrf
                    <h3 class="box-title m-b-20">Sign In</h3>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control {{ $errors->has('phone') || $errors->has('email') ? ' is-invalid' : '' }}"
                                   id="login" name="login" value="{{ old('phone') ?: old('phone') }}"
                                   required type="text" required="" placeholder="{{ __('Email or Phone') }}">
                                @if ($errors->has('phone') || $errors->has('email'))
                                    <span class="invalid-feedback text-danger" role="alert">
                                <strong>{{ $errors->first('phone') ?: $errors->first('email') }}</strong>
                            </span>
                                @endif
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control @error('password') is-invalid @enderror" id="password" name="password"
                                   type="password" required="" placeholder="{{ __('Password') }}">
                            @error('password')
                            <span class="invalid-feedback text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="checkbox checkbox-primary p-t-0">
                                <input id="remember" name="remember" type="checkbox">
                                <label for="checkbox-signup"> {{ __('Remember me') }}</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Sign In</button>
                        </div>
                    </div>

                    @if (Route::has('register'))
                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">
                            <p>Don't have an account? <a href="{{ route('register') }}" class="text-primary m-l-5"><b>Sign Up</b></a></p>
                        </div>
                    </div>
                    @endif
                </form>
            </div>
        </div>
    </section>
@endsection
