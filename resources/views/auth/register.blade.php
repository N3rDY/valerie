@extends('auth.auth')

@section('content')
    <section id="wrapper" class="login-register">
        <div class="login-box">
            <div class="white-box">
                <form class="form-horizontal form-material" id="registerform" action="{{ route('register') }}"
                      method="post">
                    @csrf
                    <h3 class="box-title m-b-20">{{ __('Sign In') }}</h3>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control @error('name') is-invalid @enderror" type="text"
                                   required="" id="name" name="name" value="{{ old('name') }}"
                                   placeholder="{{ __('Name') }}">
                            @error('name')
                            <span class="invalid-feedback text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control @error('email') is-invalid @enderror" type="email" required=""
                                   id="email" name="email" value="{{ old('email') }}"
                                   autocomplete="email"
                                   placeholder="{{ __('Email') }}">
                            @error('email')
                            <span class="invalid-feedback text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control @error('password') is-invalid @enderror"
                                   name="password" id="password" autocomplete="new-password"
                                   type="password" required="" placeholder="{{ __('Password') }}">
                            @error('password')
                            <span class="invalid-feedback text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control"
                                   name="password_confirmation" id="password_confirmation" autocomplete="new-password"
                                   type="password" required="" placeholder="{{ __('Confirm Password') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="checkbox checkbox-primary p-t-0">
                                <input id="checkbox-signup" required type="checkbox">
                                <label for="checkbox-signup"> I agree to all <a href="#">Terms</a></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light"
                                    type="submit">Sign Up
                            </button>
                        </div>
                    </div>
                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">
                            <p>Already have an account? <a href="{{ route('login')  }}" class="text-primary m-l-5"><b>Sign
                                        In</b></a></p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
