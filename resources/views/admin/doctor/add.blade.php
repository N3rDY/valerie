@extends('admin.app')

@section('content')

    <div class="row">

        <div class="col-md-12">
            @if ($message = Session::get('success'))

                <div class="alert alert-success alert-block">

                    <button type="button" class="close" data-dismiss="alert">×</button>

                    <strong>{{ $message }}</strong>

                </div>

            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

        <div class="col-sm-12">
            <form class="form-material form-horizontal" method="post" action="{{ route('admin.doctor.new')  }}">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title">Basic Information</h3>
                            @csrf
                            <div class="form-group">
                                <label class="col-md-12" for="example-text">Name
                                </label>
                                <div class="col-md-12">
                                    <input type="text" id="name" name="name" class="form-control" placeholder="Enter doctor's name" style="cursor: auto;"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12" for="bdate">Date of Birth
                                </label>
                                <div class="col-md-12">
                                    <input type="text" id="bdate" name="bdate" class="form-control datePicker" placeholder="enter your birth date"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Gender</label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="gender" name="gender">
                                        <option value="">Select Gender</option>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12" for="special">Speciality
                                </label>
                                <div class="col-md-12">
                                    <input type="text" id="speciality" name="speciality" class="form-control" placeholder="e.g. Dentist"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Description</label>
                                <div class="col-md-12">
                                    <textarea id="description" name="description" class="form-control" rows="3"></textarea>
                                </div>
                            </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="white-box">
                        <h3 class="box-title">Doctor's Account Information</h3>
                        <div class="form-group">
                            <label class="col-md-12" for="example-email">Email
                            </label>
                            <div class="col-md-12">
                                <input type="email" id="email" name="email" class="form-control" placeholder="enter your email"> </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12" for="pwd">Password
                            </label>
                            <div class="col-md-12">
                                <input type="password" id="password" name="password" class="form-control" placeholder="enter your password"> </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12" for="password_confirmation">Confirm Password
                            </label>
                            <div class="col-md-12">
                                <input type="password" id="password_confirmation" name="password_confirmation" class="form-control" placeholder="confirm your password"> </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="white-box">
                        <h3 class="box-title">Doctor's Contact Information</h3>
                            <div class="form-group">
                                <label class="col-md-12" for="furl">Address
                                </label>
                                <div class="col-md-12">
                                    <input type="text" id="addr" name="addr" class="form-control"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12" for="example-phone">Phone
                                </label>
                                <div class="col-md-12">
                                    <input type="text" id="phone" name="phone" class="form-control" placeholder="enter your phone" data-mask="(999) 999-9999"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12" for="url">Website URL
                                </label>
                                <div class="col-md-12">
                                    <input type="text" id="url" name="url" class="form-control" placeholder="your website"> </div>
                            </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="row">
                        <div class="col-lg-2 col-sm-4 col-xs-12">
                            <button type="submit" class="btn btn-block btn-info waves-effect waves-light m-r-10">Save Doctor</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
