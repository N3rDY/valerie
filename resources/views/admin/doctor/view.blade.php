@extends('admin.app')

@section('content')

    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="white-box">
                <div class="user-bg"> <img alt="user" src="{{ asset('plugins/images/big/d2.jpg')  }}" width="100%"> </div>
                <div class="user-btm-box">
                    <!-- .row -->
                    <div class="row text-center m-t-10">
                        <div class="col-md-6 b-r"><strong>Name</strong>
                            <p>{{ $doctor->name  }}</p>
                        </div>
                        <div class="col-md-6"><strong>Designation</strong>
                            <p>{{ $doctor->doctor->speciality  }}</p>
                        </div>
                    </div>
                    <!-- /.row -->
                    <hr>
                    <!-- .row -->
                    <div class="row text-center m-t-10">
                        <div class="col-md-6 b-r"><strong>Email ID</strong>
                            <p>{{ $doctor->email  }}</p>
                        </div>
                        <div class="col-md-6"><strong>Phone</strong>
                            <p>{{ $doctor->phone  }}</p>
                        </div>
                    </div>
                    <!-- /.row -->
                    <hr>
                    <!-- .row -->
                    <div class="row text-center m-t-10">
                        <div class="col-md-12"><strong>Address</strong>
                            <p>{{ $doctor->address  }}
                                <br> {{ $doctor->town  }}
                                <br> {{ $doctor->doctor->city->name  }}</p>
                        </div>
                    </div>
                    <hr>

{{--                    <div class="col-md-6 col-sm-6 text-center">--}}
{{--                        <a href="{{ route('admin.clinic.assign', $clinic->id)  }}" class="btn btn-block btn-info"> Assign Doctor to Clinic</a>--}}
{{--                    </div>--}}

{{--                    <div class="col-md-6 col-sm-6 text-center">--}}
{{--                        <a href="{{ route('admin.clinic.remove', $clinic->id)  }}" class="btn btn-block btn-danger"> Delete Doctor from Clinic</a>--}}
{{--                    </div>--}}
                    <!-- /.row -->
                </div>
            </div>
        </div>
        <div class="col-md-8 col-xs-12">
            <div class="white-box">
                <!-- .tabs -->
                <ul class="nav nav-tabs tabs customtab">
                    <li class="active tab">
                        <a href="#about" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Available Times</span> </a>
                    </li>
                </ul>
                <!-- /.tabs -->
                <div class="tab-content">
                    <!-- .tabs 1 -->
                    <div class="tab-pane active" id="about">

                        @if( $doctor->availability ) {
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Day</th>
                                    <th>Open Time</th>
                                    <th>Close Time</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach( unserialize( $doctor->availability->day ) as $d )
                                    <tr>
                                        <td>{{ $d  }}</td>
                                        <td>{{ $doctor->availability->start_time  }}</td>
                                        <td>{{ $doctor->availability->end_time  }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> No times for the doctor
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
