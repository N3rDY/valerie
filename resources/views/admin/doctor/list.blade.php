@extends('admin.app')

@section('content')

    @if(  count($doctors) <= 0 )
        <div class="alert alert-danger">
            <strong>OOh!</strong> No doctors have been added just yet
        </div>
    @else
        <div class="row">
            @foreach( $doctors as $doc )
                <!-- .col -->
                <div class="col-md-4 col-sm-4">
                    <div class="white-box">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 text-center">
                                <a href="{{ route('admin.doctor.view', $doc->user_id) }}"><img src="{{ asset('plugins/images/users/d1.jpg')  }}" alt="user" class="img-circle img-responsive"></a>
                            </div>
                            <div class="col-md-8 col-sm-8">
                                <h3 class="box-title m-b-0">{{ $doc->id }}</h3> <small>{{ $doc->id  }}</small>
                                <p> </p><address>
                                    {{ $doc->address  }}<br><br>
                                    <abbr title="Phone">P:</abbr> {{ $doc->id  }}
                                </address> <p></p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col -->
            @endforeach
        </div>
    @endif;

    <?php echo $doctors->render(); ?>

@endsection
