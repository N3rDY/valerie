@extends('admin.app')

@section('content')

    <div class="row">
        <div class="col-md-3 col-sm-6">
            <div class="white-box">
                <div class="r-icon-stats"> <i class="ti-user bg-megna"></i>
                    <div class="bodystate">
                        <h4>{{ count( $tot_users->where('role_id','=',2) )  }}</h4> <span class="text-muted">Doctors</span> </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="white-box">
                <div class="r-icon-stats"> <i class="ti-user bg-info"></i>
                    <div class="bodystate">
                        <h4>{{ count( $tot_users->where('role_id','=',3) )  }}</h4> <span class="text-muted">Patients</span> </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="white-box">
                <div class="r-icon-stats"> <i class="ti-user bg-success"></i>
                    <div class="bodystate">
                        <h4>{{ count( $clinics )  }}</h4> <span class="text-muted">Clinics</span> </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="white-box">
                <div class="r-icon-stats"> <i class="ti-user bg-inverse"></i>
                    <div class="bodystate">
                        <h4>{{ count( $pharmacies ) }}</h4> <span class="text-muted">Pharmacies</span> </div>
                </div>
            </div>
        </div>
    </div>

@endsection
