@extends('admin.app')

@section('content')

    <div class="row">

        <div class="col-md-12">
            @if ($message = Session::get('success'))

                <div class="alert alert-success alert-block">

                    <button type="button" class="close" data-dismiss="alert">×</button>

                    <strong>{{ $message }}</strong>

                </div>

            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

        <div class="col-sm-12">
            <form class="form-material form-horizontal" method="post" action="{{ route('admin.clinic.new')  }}">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title">Clinic Information</h3>
                            @csrf
                            <div class="form-group">
                                <label class="col-md-12" for="example-text">Name
                                </label>
                                <div class="col-md-12">
                                    <input type="text" id="name" name="name" class="form-control" value="{{ old('name')  }}" placeholder="Enter name of clinic" style="cursor: auto;"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12" for="address">Address
                                </label>
                                <div class="col-md-12">
                                    <input type="text" id="address" name="address" class="form-control" value="{{ old('address')  }}" placeholder="e.g. 123 Seke"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12" for="town">Town
                                </label>
                                <div class="col-md-12">
                                    <input type="text" id="town" name="town" class="form-control" value="{{ old('town')  }}" placeholder="e.g. Chitungwiza"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">City</label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="city" name="city">
                                        <option value="">Select City</option>
                                        @foreach( $cities as $city )
                                            <option value="{{ $city->id  }}">{{ $city->name  }} - {{ $city->code  }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12" for="phone">Phone
                                </label>
                                <div class="col-md-12">
                                    <input type="text" id="phone" name="phone" value="{{ old('phone')  }}" class="form-control" placeholder="011 125 2563"> </div>
                            </div>
                        <div class="form-group">
                            <label class="col-md-12" for="email">Email
                            </label>
                            <div class="col-md-12">
                                <input type="text" id="email" name="email" value="{{ old('email')  }}" class="form-control" placeholder="example@example.com"> </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">About</label>
                            <div class="col-md-12">
                                <textarea id="about" name="about" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="row">
                        <div class="col-lg-2 col-sm-4 col-xs-12">
                            <button type="submit" class="btn btn-block btn-info waves-effect waves-light m-r-10">Add Clinic</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
