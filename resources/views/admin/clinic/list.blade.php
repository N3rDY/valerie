@extends('admin.app')

@section('content')

        <div class="row">

            <div class="col-md-12">
                @if ($message = Session::get('success'))

                    <div class="alert alert-success alert-block">

                        <button type="button" class="close" data-dismiss="alert">×</button>

                        <strong>{{ $message }}</strong>

                    </div>

                @endif

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>

            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Clinic List</h3>
                    <p class="text-muted">&nbsp;</p>
                        @if(  count($clinics) <= 0 )
                            <div class="alert alert-danger">
                                <strong>OOh!</strong> No clinics have been added just yet
                            </div>
                        @else
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Phone</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach( $clinics as $clinic )
                                    <tr>
                                        <td>{{ $clinic->id  }}</td>
                                        <td>{{ $clinic->name  }}</td>
                                        <td>
                                            {{ $clinic->address  }}
                                            {{ $clinic->town  }}

                                        </td>
                                        <td>{{ $clinic->phone  }}</td>
                                        <td>
                                            <a href="{{ route('admin.clinic.view',$clinic->id)}}" class="btn btn-rounded btn-info">View</a>
                                            <a href="{{ route('admin.clinic.doctors',$clinic->id)}}" class="btn btn-rounded btn-success">Doctors</a>
                                        </td>
                                        <td>
                                            <form action="{{ route('admin.clinic.destroy', $clinic->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-rounded btn-danger" type="submit">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @endif
                </div>
            </div>

        </div>

    <?php //echo $clinics->render(); ?>

@endsection
