@extends('admin.app')

@section('content')

        <div class="row">

            <div class="col-md-12">
                @if ($message = Session::get('success'))

                    <div class="alert alert-success alert-block">

                        <button type="button" class="close" data-dismiss="alert">×</button>

                        <strong>{{ $message }}</strong>

                    </div>

                @endif

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>

            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Clinic Doctors List</h3>
                </div>
            </div>

            <div class="col-sm-12">

                @if(  count($doctors) <= 0 )
                    <div class="alert alert-danger">
                        <strong>OOh!</strong> No doctors have been added to this clinic
                    </div>
                @else
                    @foreach( $doctors as $doctor )

                        <div class="col-md-4 col-sm-4">
                            <div class="white-box">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 text-center">
                                        <a href="{{ route('admin.doctor.view', $doctor->user->id) }}"><img src="{{ asset('plugins/images/users/d1.jpg')  }}" alt="user" class="img-circle img-responsive"></a>
                                    </div>
                                    <div class="col-md-8 col-sm-8">
                                        <h3 class="box-title m-b-0">{{ $doctor->user->name  }}</h3> <small>{{ $doctor->profile->speciality  }}</small>
                                        <p> </p><address>
                                            {{ $doctor->user->address  }}<br><br>
                                            <abbr title="Phone">P:</abbr> {{ $doctor->user->phone  }}
                                        </address> <p></p>
                                    </div>
{{--                                    <p>&nbsp;</p>--}}
{{--                                    <button type="button" id="doctorTimesBtn" class="btn btn-block btn-primary" data-toggle="modal" data-doctor-day="{{ $doctor->day  }}" data-doctor-start="{{ $doctor->start_time  }}" data-doctor-end="{{ $doctor->end_time  }}" data-target="#doctorTimes" data-whatever="@mdo">Show Times</button>--}}
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif

            </div>

            <?php echo $doctors->render(); ?>

            <div class="modal fade" id="doctorTimes" tabindex="-1" role="dialog" aria-labelledby="doctorTimesLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="doctorTimesLabel">Doctor Times</h4> </div>
                        <div class="modal-body">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <?php echo $doctors->render(); ?>

@endsection
