@extends('admin.app')

@section('content')

    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="white-box">
                <div class="user-bg"> <img alt="user" src="{{ asset('plugins/images/big/d2.jpg')  }}" width="100%"> </div>
                <div class="user-btm-box">
                    <!-- .row -->
                    <div class="row text-center m-t-10">
                        <div class="col-md-6 b-r"><strong>Name</strong>
                            <p>{{ $clinic->name  }}</p>
                        </div>
                        <div class="col-md-6"><strong>Designation</strong>
                            <p>{{ $clinic->name  }}</p>
                        </div>
                    </div>
                    <!-- /.row -->
                    <hr>
                    <!-- .row -->
                    <div class="row text-center m-t-10">
                        <div class="col-md-6 b-r"><strong>Email ID</strong>
                            <p>{{ $clinic->email  }}</p>
                        </div>
                        <div class="col-md-6"><strong>Phone</strong>
                            <p>{{ $clinic->phone  }}</p>
                        </div>
                    </div>
                    <!-- /.row -->
                    <hr>
                    <!-- .row -->
                    <div class="row text-center m-t-10">
                        <div class="col-md-12"><strong>Address</strong>
                            <p>{{ $clinic->address  }}
                                <br> {{ $clinic->town  }}
                                <br> {{ $clinic->city_id  }}</p>
                        </div>
                    </div>
                    <hr>

                    <div class="col-md-6 col-sm-6 text-center">
                        <a href="{{ route('admin.clinic.assign', $clinic->id)  }}" class="btn btn-block btn-info"> Assign Doctor to Clinic</a>
                    </div>

                    <div class="col-md-6 col-sm-6 text-center">
                        <a href="{{ route('admin.clinic.remove', $clinic->id)  }}" class="btn btn-block btn-danger"> Delete Doctor from Clinic</a>
                    </div>
                    <!-- /.row -->
                </div>
            </div>
        </div>
        <div class="col-md-8 col-xs-12">
            <div class="white-box">
                <!-- .tabs -->
                <ul class="nav nav-tabs tabs customtab">
                    <li class="active tab">
                        <a href="#about" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">About</span> </a>
                    </li>
                    <li class="tab">
                        <a href="#update" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Update Details</span> </a>
                    </li>
                </ul>
                <!-- /.tabs -->
                <div class="tab-content">
                    <!-- .tabs 1 -->
                    <div class="tab-pane active" id="about">
                        <p>{{ $clinic->about  }} </p>
                    </div>
                    <!-- .tabs 2 -->
                    <div class="tab-pane" id="update">
                        <form class="form-material form-horizontal">
                            <div class="form-group">
                                <label class="col-md-12" for="example-text">Name
                                </label>
                                <div class="col-md-12">
                                    <input type="text" id="example-text" name="example-text" class="form-control" placeholder="enter your name" value="Jonathan Doe"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12" for="bdate">Date of Birth
                                </label>
                                <div class="col-md-12">
                                    <input type="text" id="bdate" name="bdate" class="form-control mydatepicker" placeholder="enter your birth date" value="12/10/2017"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Gender</label>
                                <div class="col-sm-12">
                                    <select class="form-control">
                                        <option>Select Gender</option>
                                        <option selected="selected">Male</option>
                                        <option>Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Profile Image</label>
                                <div class="col-sm-12"> <img class="img-responsive" src="../plugins/images/big/d2.jpg" alt="" style="max-width:120px;"> </div>
                                <div class="col-sm-12">
                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                        <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="..."> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12" for="special">Speciality
                                </label>
                                <div class="col-md-12">
                                    <input type="text" id="special" name="special" class="form-control" placeholder="e.g. Dentist" value="Neurosurgeon"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Description</label>
                                <div class="col-md-12">
                                    <textarea class="form-control" rows="3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12" for="url">Website URL
                                </label>
                                <div class="col-md-12">
                                    <input type="text" id="url" name="url" class="form-control" placeholder="your website" value="http://www.example-website.com"> </div>
                            </div>
                            <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Submit</button>
                            <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                        </form>
                    </div>
                    <!-- /.tabs 3 -->
                </div>
            </div>
        </div>
    </div>
@endsection
