@extends('admin.app')

@section('content')

    <div class="row">

        <div class="col-md-12">
            @if ($message = Session::get('success'))

                <div class="alert alert-success alert-block">

                    <button type="button" class="close" data-dismiss="alert">×</button>

                    <strong>{{ $message }}</strong>

                </div>

            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

        <div class="col-sm-12">
            <form class="form-material form-horizontal" method="post" action="{{ route('admin.clinic.assign', $clinic->id)  }}">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title">Assign Doctor to Clinic</h3>
                            @csrf
                            <div class="form-group">
                                <label class="col-md-12" for="example-text">Clinic Name
                                </label>
                                <div class="col-md-12">
                                    <input type="text" disabled class="form-control" value="{{ $clinic->name  }}" style="cursor: auto;"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12" for="address">City Name
                                </label>
                                <div class="col-md-12">
                                    <input type="text" disabled class="form-control" value="{{ $city->name  }}"> </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12">Doctors</label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="doctor" name="doctor">
                                        <option value="">Select Doctor</option>
                                    @foreach( $doctors as $doc )
                                        <option value="{{ $doc->user_id  }}">{{ $doc->user->name  }} - {{ $doc->speciality  }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Available Days</label>
                                <div class="col-md-12">
                                    <div class="radio-list">
                                        <label>
                                            <input type="checkbox" name="daylist[]" value="Monday"> Monday </label>
                                        <label>
                                            <input type="checkbox" name="daylist[]" value="Tuesday"> Tuesday </label>
                                        <label>
                                            <input type="checkbox" name="daylist[]" value="Wednesday"> Wednesday </label>
                                        <label>
                                            <input type="checkbox" name="daylist[]" value="Thursday"> Thursday </label>
                                        <label>
                                            <input type="checkbox" name="daylist[]" value="Friday"> Friday </label>
                                        <label>
                                            <input type="checkbox" name="daylist[]" value="Saturday"> Saturday </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12" for="address">Start Time
                                </label>
                                <div class="col-md-6">
                                    <input type="time" name="start_time" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12" for="address">End Time
                                </label>
                                <div class="col-md-6">
                                    <input type="time" name="end_time" class="form-control">
                                </div>
                            </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="row">
                        <div class="col-lg-2 col-sm-4 col-xs-12">
                            <button type="submit" class="btn btn-block btn-info waves-effect waves-light m-r-10">Assign Doctor</button>
                            <input type="hidden" name="city_id" class="form-control" value="{{ $city->id  }}">
                            <input type="hidden" name="clinic_id" class="form-control" value="{{ $clinic->id  }}">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
