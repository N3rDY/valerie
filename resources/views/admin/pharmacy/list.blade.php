@extends('admin.app')

@section('content')

        <div class="row el-element-overlay">

            @if(  count($pharmacies) <= 0 )
                <div class="alert alert-danger">
                    <strong>OOh!</strong> No clinics have been added just yet
                </div>
        @else

            @foreach( $pharmacies as $pharmacy )
            <!-- .usercard -->
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                <div class="white-box">
                    <div class="el-card-item">
                        <div class="el-card-avatar el-overlay-1"> <img src="{{ asset('avatars') }}/{{ $pharmacy->photo }}">
                            <div class="el-overlay">
                                <ul class="el-info">
                                    <li><a class="btn default btn-outline image-popup-vertical-fit" href="tel:{{ $pharmacy->phone  }}"><i class="icon-magnifier"></i></a></li>
                                    <li><a class="btn default btn-outline" href="maito:{{ $pharmacy->email  }}"><i class="icon-link"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="el-card-content">
                            <h3 class="box-title">{{ $pharmacy->name  }}</h3> <small>{{ $pharmacy->address  }}</small>
                            <br> <small>{{ $pharmacy->town  }}</small>
                            <br> <small>{{ $pharmacy->city_id  }}</small> </div>
                    </div>
                </div>
            </div>
            @endforeach

            @endif
            <!-- /.usercard-->
        </div>
    <?php echo $pharmacies->render(); ?>

@endsection
