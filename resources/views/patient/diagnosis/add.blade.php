@extends('patient.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))

                <div class="alert alert-success alert-block">

                    <button type="button" class="close" data-dismiss="alert">×</button>

                    <strong>{{ $message }}</strong>

                </div>

            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title">Diagnose</h3>
            <form class="form-material form-horizontal" _lpchecked="1" method="post" action="{{ route('patient.diagnosis.new') }}">
                @csrf
                <div class="row form-group">

                    <div class="col-sm-4">
                        <label class="col-sm-12">City</label>
                        <select class="form-control" id="city" name="city">
                            <option value="" selected>Select city</option>
                            @foreach( $cities as $city )
                                <option value="{{ $city->id }}">{{ $city->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-sm-4">
                        <label class="col-sm-4">Sex</label>
                        <div class="col-sm-12">
                            <select class="form-control" id="sex" name="sex">
                                <option value="">Select Service</option>
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <label class="col-sm-4">Age</label>
                        <div class="col-sm-12">
                            <input type="number" min="0" id="age" name="age" class="form-control" placeholder="enter your age">
                        </div>
                    </div>

                </div>

                <div class="row form-group">

                    <div class="col-sm-4">
                        <label class="col-sm-4">Search Symptoms</label>
                        <div class="col-sm-12">
                            <input type="text" id="search" name="search" class="form-control" placeholder="enter search here">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <label class="col-sm-12">Choose Symptoms</label>
                        <div class="col-sm-12">
                            <select class="form-control" multiple="" id="symptoms" name="symptoms[]">
                            </select>
                        </div>

                        <button type="button" id="add_item" class="btn btn-inverse waves-effect waves-light">Add Selected</button>

                    </div>
                    <div class="col-sm-4">
                        <label class="col-sm-12">Selected Symptoms</label>
                        <div class="col-sm-12">
                            <input type="text" name="selected_symptoms" id="selected_symptoms" value="" data-role="tagsinput" placeholder="">
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Save Search</button>
            </form>
        </div>
    </div>
</div>

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <h3 class="box-title">Diagnosis</h3>
                <button type="button" id="diagnosis_btn" class="btn btn-info waves-effect waves-light m-r-10"> Diagnose</button>

                <div class="diagnosis_result" id="diagnosis_result">

                </div>
            </div>
        </div>
    </div>

@endsection
