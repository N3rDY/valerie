@extends('patient.app')

@section('content')

    <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class="white-box">
                <div class="r-icon-stats"> <i class="ti-user bg-megna"></i>
                    <div class="bodystate">
                        <h4>{{ $appointments->count() }}</h4> <span class="text-muted">Appointments</span> </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class="white-box">
                <div class="r-icon-stats"> <i class="ti-user bg-info"></i>
                    <div class="bodystate">
                        <h4>{{ $diagnosis->count() }}</h4> <span class="text-muted">Diagnosis</span> </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class="white-box">
                <div class="r-icon-stats"> <i class="ti-user bg-success"></i>
                    <div class="bodystate">
                        <h4></h4> <span class="text-muted">Pharmacy Orders</span> </div>
                </div>
            </div>
        </div>
    </div>

@endsection
