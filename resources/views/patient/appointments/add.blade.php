@extends('patient.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))

                <div class="alert alert-success alert-block">

                    <button type="button" class="close" data-dismiss="alert">×</button>

                    <strong>{{ $message }}</strong>

                </div>

            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title">Book An Appointment</h3>
            <form class="form-material form-horizontal" _lpchecked="1" method="post" action="{{ route('patient.diagnosis.book') }}">
                @csrf
                <div class="row form-group">

                    <div class="col-sm-4">
                        <label class="col-sm-12">City</label>
                        <select class="form-control" id="city" name="city">
                            <option value="" selected>Select city</option>
                            @foreach( $cities as $city )
                                <option value="{{ $city->id }}">{{ $city->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-sm-4">
                        <label class="col-sm-4">Clinic</label>
                        <div class="col-sm-12">
                            <select class="form-control" id="clinic" name="clinic">
                                <option value="">Select Service</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <label class="col-sm-4">Doctor</label>
                        <div class="col-sm-12">
                            <select class="form-control" name="doctor" id="doctor">
                                <option value="">Select doctor</option>>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-12">Service</label>
                    <div class="col-sm-12">
                        <select class="form-control" id="service" name="service">
                            <option value="">Select Service</option>
                            <option value="Dental Checkup">Dental Checkup</option>
                            <option value="Full Body Checkup">Full Body Checkup</option>
                            <option value="ENT Checkup">ENT Checkup</option>
                            <option value="Heart Checkup">Heart Checkup</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12" for="date-time">Date Time<span class="text-muted font-12 p-l-20">(e.g. "12/10/2017 05:15 pm")</span></label>
                    <div class="col-md-12">
                        <input type="date" min="{{ date('Y-m-d') }}" max="{{ date('Y-m-d',strtotime('+7 day')) }}" id="book_date" name="book_date" class="form-control" placeholder="enter date and time of appointment" data-mask="99/99/9999 99:99 am">
                    </div>
                </div>
                <div class="col-md-12 hidden availabilityStatus">
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong class="message"></strong>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Note</label>
                    <div class="col-md-12">
                        <textarea id="notes" name="notes" class="form-control" rows="3"></textarea>
                    </div>
                </div>
                <button type="submit" class="btn btn-info waves-effect waves-light m-r-10">Submit</button>
                <button type="submit" class="btn btn-inverse waves-effect waves-light">Cancel</button>
            </form>
        </div>
    </div>
</div>

@endsection
