@extends('patient.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))

                <div class="alert alert-success alert-block">

                    <button type="button" class="close" data-dismiss="alert">×</button>

                    <strong>{{ $message }}</strong>

                </div>

            @endif

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

        <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title">Appointments</h3>
                    <p class="text-muted">List of all the bookings you have made</p>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Date Of Appointment</th>
                                <th>Doctor Name</th>
                                <th>Clinic Name</th>
                                <th>Status</th>
                                <th></th>
                                <th>Booked On</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach( $appointments as $book )
                            <tr>
                                <td>{{ $book->date_of_visit }}</td>
                                <td>{{ $book->user->name }}</td>
                                <td>{{ $book->clinic->name }}</td>
                                <td>{{ $book->status }}</td>
                                <td></td>
                                <td>{{ $book->created_at }}</td>
                                <td><a class="btn btn-danger btn-rounded">Cancel</a> </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

    </div>

@endsection
