<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_no', 'name', 'email', 'phone','bdate','gender','address', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles(){
        return $this->belongsToMany('App\Role');
    }

    public function doctors(){
        return $this->belongsTo('App\Doctor' );
    }

    public function doctor(){
        return $this->hasOne('App\Doctor' );
    }

    public function availability(){
        return $this->hasOne('App\DoctorAvailability', 'doctor_id' );
    }

    public function appointments(){
        return $this->hasMany('App\Appointment', 'user_id');
    }

    public function diagnosis(){
        return $this->hasMany('App\Diagnosis', 'user_id');
    }
}
