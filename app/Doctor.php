<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    //
    protected $fillable = [
        'user_id', 'speciality', 'description', 'url','city_id',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function city(){
        return $this->hasOne('App\City', 'id');
    }
}
