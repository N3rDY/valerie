<?php


namespace App\Utils;


class Helper {

    public static $start = 0;
    public static $length = 12;
    public static $str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';


    /**
     * Geneate unique patient identifier/number
     * @param $a
     * @param $b
     * @return String
     */
    public static function genPatientNumber(){;
        return strtoupper( substr( str_shuffle( self::$str ), self::$start, self::$length ) );
    }

}