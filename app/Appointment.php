<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    //

    public function clinic(){
        return $this->belongsTo('App\Clinic');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

}
