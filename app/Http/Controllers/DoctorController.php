<?php

namespace App\Http\Controllers;

use App\Clinic;
use App\Doctor;
use App\DoctorAvailability;
use App\Http\Requests\DoctorStoreRequest;
use App\Pharmacy;
use App\User;
use App\UserRole;
use App\Utils\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class DoctorController extends Controller
{
    //
    public function showAddForm(){
        return view('admin.doctor.add');
    }

    public function add( Request $req ){

        $userData = array(
            'user_no' => Helper::genPatientNumber(),
            'name' => $req->name,
            'email' => $req->email,
            'password' => Hash::make($req->password),

            // Custom fields
            'phone' => $req->phone,
            'bdate' => $req->bdate,
            'gender' => $req->gender,
            'address' => $req->address,
        );

        $user = User::create($userData);

        if ( $user->id ) {

            // Populate User Role Array
            $role['user_id'] = $user->id;
            $role['role_id'] = 2;

            $profile['user_id'] = $user->id;
            $profile['speciality'] = $req->speciality;
            $profile['description'] = $req->description;
            $profile['url'] = $req->url;

            UserRole::firstOrCreate($role);
            Doctor::firstOrCreate($profile);

            return back()->with('success', 'Doctor Added');
        }else{
            return back()->withErrors('Oops, Something went wrong there');
        }

    }

    public function list(){
        $users = new User();
        //$doctors = $users->doctors();
        $doctors = UserRole::where('user_id', '=', 2)->paginate(1);
        return view('admin.doctor.list', compact('doctors'));
    }

    public function view( $id ){
        $doctor = User::find($id);
        return view('admin.doctor.view', compact('doctor', 'times'));
    }

    public function schedule(){
        return view('admin.doctor.schedule');
    }
}
