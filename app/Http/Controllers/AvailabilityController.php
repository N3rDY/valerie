<?php

namespace App\Http\Controllers;

use App\Doctor;
use App\DoctorAvailability;
use App\User;
use Illuminate\Http\Request;

class AvailabilityController extends Controller
{
    //

    public function getDoctors( Request $req ){
        $response = array();
        $data = array();
        $val = DoctorAvailability::where('clinic_id', '=', $req->clinic_id)->get();

        foreach($val as $v) {
            $doctor = $v->user;
            $profile =  $v->profile;

            $response['doctor_id'] = $v->doctor_id;
            $response['doctor_name'] = $doctor->name;

            $data[] = $response;
        }

        return response()->json($data);
    }

    public function getAvailability( Request $req ){
        $timestamp = strtotime($req->book_date);
        $compareday = date("l", $timestamp);
        $flag=0;
        $message = '';

        $dates = DoctorAvailability::where('clinic_id', '=', $req->clinic_id)->where('doctor_id', '=', $req->doctor_id)->first();

        foreach( unserialize( $dates->day) as $day ) {
            if( $day == $compareday ) {
                $flag++;
                $message =  "Doctor Available on ".$compareday;
                break;
            }
        }
        if($flag==0)
            $message = "Doctor Unavailable on ".$compareday;

        return response()->json( $message );
    }
}
