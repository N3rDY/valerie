<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use App\UserRole;
use App\Utils\Helper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'user_no' => Helper::genPatientNumber(),
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        if ( $user->id ) {

            // Populate User Role Array
            $role['user_id'] = $user->id;
            $role['role_id'] = 3;

            UserRole::firstOrCreate($role);
        }

        return $user;
    }

    protected function redirectTo(){

        if( $this->isUserRole('admin') ) {
            //dd(('admin'));
            return route('admin.dashboard');
        }else if( $this->isUserRole('doctor') ){
            //dd(('principle'));
            return route('doctor.dashboard');
        }else if( $this->isUserRole('patient') ){
            //dd(('agent'));
            return route('patient.dashboard');
        }else{
            return "Error";
        }
    }

    private function isUserRole( $roleName ){
        if( Auth::check() ) {
            $userRoles = Auth::User()->roles->pluck('name');
            return $userRoles->contains($roleName);
        }
    }

}
