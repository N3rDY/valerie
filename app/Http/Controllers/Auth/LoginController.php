<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    protected $phone;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');

        $this->phone = $this->findPhoneNumber();
    }

    protected function redirectTo(){

        if( $this->isUserRole('admin') ) {
            //dd(('admin'));
            return route('admin.dashboard');
        }else if( $this->isUserRole('doctor') ){
            //dd(('doctor'));
            return route('doctor.dashboard');
        }else if( $this->isUserRole('patient') ){
            //dd(('patient'));
            return route('patient.dashboard');
        }else{
            return "Error";
        }
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'login'    => 'required',
            'password' => 'required',
        ]);

        $login_type = filter_var($request->input('login'), FILTER_VALIDATE_EMAIL )
            ? 'email'
            : 'phone';

        $request->merge([
            $login_type => $request->input('login')
        ]);

        if (Auth::attempt($request->only($login_type, 'password'))) {
            return redirect()->intended($this->redirectPath());
        }

        return $this->sendFailedLoginResponse($request);
    }


    private function isUserRole( $roleName ){
        if( Auth::check() ) {
            $userRoles = Auth::User()->roles->pluck('name');
            return $userRoles->contains($roleName);
        }
    }

    public function findPhoneNumber(){
        $login = request()->input('login');
        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
        request()->merge([$fieldType => $login]);

        return $fieldType;
    }
}
