<?php

namespace App\Http\Controllers;

use App\Services\ApiClient;
use Illuminate\Http\Request;

class DiagnosisController extends Controller
{
    //

    public function getSymptoms(Request $req){
        $client = new ApiClient();
        $res = $client->search($req->search);

        return json_decode( $res->getBody()->getContents() );
    }

    public function getConditions(Request $req){
        $client = new ApiClient();
        $ev = array();
        $r = array();
        $evidence = explode(',', $req->symptoms);

        foreach( $evidence as $e ) {
            $ev['id'] = $e;
            $ev['initial'] = true;
            $ev['related'] = true;
            $ev['choice_id'] = 'present';

            $r[] = $ev;
        }

        $body = array(
            "sex" => $req->sex,
            "age" => $req->age,
            "evidence" => $r
        );

        //dd(json_encode($body));

        $res = $client->diagnose(json_encode($body));

        return json_decode( $res->getBody()->getContents() )->conditions;
    }


}
