<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\Requests\PharmacyStoreRequest;
use App\Pharmacy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PharmacyController extends Controller
{
    //

    public function __construct() {
        $this->middleware(['auth']);
    }

    public function showAddForm(){
        $cities = City::all();
        return view('admin.pharmacy.add', compact('cities'));
    }

    public function savePharmacy(PharmacyStoreRequest $req){
        $user = Auth::User();
        $pharmacy = new Pharmacy();

        $pharmacy->name = $req->name;
        $pharmacy->address = $req->address;
        $pharmacy->town = $req->town;
        $pharmacy->city_id = $req->city;
        $pharmacy->phone = $req->phone;
        $pharmacy->email = $req->email;

        if( $req->hasFile('pic') ) {
            $fileName = 'pharmacy_avatar_'.time().'.'.$req->pic->getClientOriginalExtension();
            $req->pic->move(public_path('avatars'), $fileName);
            $pharmacy->photo = $fileName;
        }

        if ( $pharmacy->save() ) {

            return back()->with('success', 'Pharmacy Added');
        }else{
            return back()->withErrors('Oops, Something went wrong there');
        }
    }

    public function list(){
        $pharmacies = Pharmacy::paginate(10);
        return view('admin.pharmacy.list', compact('pharmacies'));
    }

    public function orders(){

    }

    public function view(){

    }


    public function destroy(){

    }
}
