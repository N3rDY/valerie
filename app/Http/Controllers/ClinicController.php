<?php

namespace App\Http\Controllers;

use App\City;
use App\Clinic;
use App\Doctor;
use App\DoctorAvailability;
use App\Http\Requests\ClinicStoreRequest;
use App\User;
use App\UserRole;
use App\Utils\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ClinicController extends Controller
{
    //

    public function showAddForm(){
        $cities = City::all();
        return view('admin.clinic.add', compact('cities'));
    }

    public function add( ClinicStoreRequest $req ){

        $clinic = new Clinic();

        $clinic->name = $req->name;
        $clinic->address = $req->address;
        $clinic->town = $req->town;
        $clinic->city_id = $req->city;
        $clinic->phone = $req->phone;
        $clinic->email = $req->email;
        $clinic->about = $req->about;

        if ( $clinic->save() ) {

            return back()->with('success', 'Clinic Added');
        }else{
            return back()->withErrors('Oops, Something went wrong there');
        }

    }

    public function list(){
        $clinics = Clinic::paginate(10);
        return view('admin.clinic.list', compact('clinics'));
    }

    public function view( $id ){
        $clinic = Clinic::findorfail($id);
        return view('admin.clinic.view', compact('clinic'));
    }

    public function schedule(){
        return view('admin.clinic.schedule');
    }

    public function destroy(Request $req, $id){
        $clinic = Clinic::findOrfail($id);
        if( $clinic->delete()){
            return back()->with('success', 'Clinic deleted successfully');
        }
    }

    public function assignForm( $id ){
        $clinic = Clinic::findOrfail($id);
        $city = City::find($clinic->city_id);
        $doctors = Doctor::where('city_id', '=', $city->id)->get();
        return view('admin.clinic.assign', compact('clinic','city', 'doctors'));
    }

    public function assign( Request $req ){
        $dv = new DoctorAvailability();
        $dv->clinic_id = $req->clinic_id;
        $dv->doctor_id = $req->doctor;
        $dv->day = serialize( $req->daylist );
        $dv->start_time = $req->start_time;
        $dv->end_time = $req->end_time;

        $doc = User::find($req->doctor);

        if( $dv->save() ){
            return back()->with('success', 'Doctor '.$doc->name.' assigned to Clinic');
        }else{

        }
    }

    public function removeForm( $id ){
        $clinic = Clinic::findOrfail($id);
        $city = City::find($clinic->city_id);

        $doctors = DoctorAvailability::where('clinic_id', '=', $id)->get();
        return view('admin.clinic.remove', compact('clinic','city', 'doctors'));
    }

    public function remove( Request $req ){
        $dv = DoctorAvailability::findOrfail($req->doctor);

        if( $dv->delete() ){
            return back()->with('success', 'Doctor '.$dv->user->name.' removed from Clinic');
        }else{
            return back()->withErrors('Error removing doctor from clinic');
        }
    }

    public function doctors( $id ){
        $doctors = DoctorAvailability::where('clinic_id', '=', $id)->paginate(10);
        return view('admin.clinic.doctors', compact('doctors'));
    }

}
