<?php

namespace App\Http\Controllers;

use App\Clinic;
use App\Pharmacy;
use App\User;
use App\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    //
    private function getAllData( $table ){
        return DB::table($table)->get();
    }

    public function __construct() {
        $this->middleware(['auth', 'admin']);
    }
    public function dashboard(){
        $tot_users = UserRole::all();
        $clinics = Clinic::all();
        $pharmacies = Pharmacy::all();
        return view('admin.index', compact('tot_users','clinics','pharmacies'));
    }
}
