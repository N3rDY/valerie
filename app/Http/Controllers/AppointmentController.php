<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AppointmentController extends Controller
{
    //

    public function bookForm(){
        $cities = City::all();
        return view('patient.appointments.add', compact('cities'));
    }

    public function book( Request $req ) {
        $book = new Appointment();

        $book->user_id = Auth::User()->id;
        $book->city_id = $req->city;
        $book->clinic_id = $req->clinic;
        $book->doctor_id = $req->doctor;
        $book->doctor_id = $req->doctor;
        $book->date_of_visit = $req->book_date;
        $book->notes = $req->notes;
        $book->status = "Waiting confirmation";

        if( $book->save() ){
            return back()->with('success', 'Appointment request sent');
        }else{
            return back()->withErrors('Something went wront. Please tru again');
        }
    }
}
