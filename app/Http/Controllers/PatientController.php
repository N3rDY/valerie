<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\City;
use App\Diagnosis;
use App\Http\Requests\StoreDiagnosisRequest;
use App\Services\ApiClient;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PatientController extends Controller
{
    //

    private $_client;

    public function __construct() {
        $this->middleware(['auth', 'patient']);
    }

    private function getAllData( $table ){
        return DB::table($table)->get();
    }
    public function dashboard(){
        $appointments = Auth::User()->appointments;
        $diagnosis = Auth::User()->diagnosis;
        return view('patient.index', [
            'appointments' => $appointments,
            'diagnosis' => $diagnosis
        ]);
    }

    public function newDiagnosis(){
        $cities = City::all();
        //$res = $client->search('smoke');

        //dd( json_decode( $res->getBody()->getContents() ) );
        return view('patient.diagnosis.add', compact('cities'));
    }

    public function storeDiagnosis(StoreDiagnosisRequest $req){
        $dg = new Diagnosis();
        $dg->user_id = Auth::User()->id;
        $dg->city_id = $req->city;
        $dg->sex = $req->sex;
        $dg->age = $req->age;
        $dg->conditions = '';
        $dg->symptoms = $req->selected_symptoms;
        //$dg->conditions = $req->conditions;

        if( $dg->save() ){
            return back()->with('sucsess', 'Sucessfully saved your request for future reference');
        }
    }

    public function getClinics( Request $req ){

    }

    public function getDoctors( Request $req ){

    }



    public function schedule(){
        $appointments = Appointment::all();
        return view('patient.appointments.schedule', compact('appointments'));
    }
}
