<?php

namespace App\Http\Controllers;

use App\Clinic;
use Illuminate\Http\Request;

class CityController extends Controller
{
    //

    public function getClinics( Request $req ){
        return response()->json(Clinic::where('city_id', '=', $req->city_id)->get());
    }
}
