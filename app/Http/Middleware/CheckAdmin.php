<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( Auth::check() ) {
            $userRoles = Auth::User()->roles->pluck('name');
            if( ! $userRoles->contains('admin') ){
                return redirect()->route('nopermission');
            }
        }
        return $next($request);
    }
}
