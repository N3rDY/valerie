<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PharmacyStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:50',
            'address' => 'required|string|max:255',
            'town' => 'required|string|max:50',
            'city' => 'required',
            'phone' => 'required|string|max:10',
            'email' => 'required|email|max:50',
            'pic' => 'required|image|mimes:jpeg,jpg,png|max:1024',

        ];
    }
}
