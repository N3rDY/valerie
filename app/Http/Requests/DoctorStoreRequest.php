<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DoctorStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users',
            'name' => 'required|string|max:50',
            'password' => 'required',

            'bdate' => 'required|date',
            'gender' => 'required|string|max:10',
            'speciality' => 'required|string|max:50',

            'description' => 'max:250',
            'phone' => 'required|string|max:10',
            'addr' => 'required',

        ];
    }
}
