<?php


namespace App\Services;


use GuzzleHttp\Client;

class ApiClient {

    const BASE_URL = "https://api.infermedica.com/v2/";
    private $_client;

    private function _getClient( $endpoint, $method, $body = null ){
        $this->_client = new Client(['base_url' => self::BASE_URL]);
        $response = $this->_client->request($method, self::BASE_URL.$endpoint, [
            'auth' => '',
            'headers' => [
                'App-Id' => 'd8d0dd78',
                'App-Key' => 'a6b3148b7cd0d0c8bf9ec9caa4549af6',
                'content-type' => 'application/json',
                'Accept' => 'application/json'
            ],
            'body' => $body
        ]);

        return $response;
    }

    public function search( $search ){
        $response = $this->_getClient('search?phrase='.$search, 'GET');
        return $response;
    }

    public function diagnose( $body ){
        $response = $this->_getClient('diagnosis', 'POST', $body);
        return $response;
    }

}
