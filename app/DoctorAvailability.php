<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorAvailability extends Model
{
    //

    public function user(){
        return $this->belongsTo('App\User', 'doctor_id');
    }

    public function profile(){
        return $this->belongsTo('App\Doctor', 'doctor_id');
    }

}
