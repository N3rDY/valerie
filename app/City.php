<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    //
    protected $fillable = [
        'name', 'code',
    ];

    public function clinic(){
        return $this->belongsToMany('App\Clinic');
    }

    public function clinics(){
        return $this->belongsToMany('App\Clinic');
    }
}
